using Nuke.Common;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tools.Unity;
using static Nuke.Common.Tools.Unity.UnityTasks;

class Build : NukeBuild
{
    /// Support plugins are available for:
    ///   - JetBrains ReSharper        https://nuke.build/resharper
    ///   - JetBrains Rider            https://nuke.build/rider
    ///   - Microsoft VisualStudio     https://nuke.build/visualstudio
    ///   - Microsoft VSCode           https://nuke.build/vscode

    public static int Main () => Execute<Build>(x => x.Export);

    [Solution]
    readonly Solution Solution;

    Target Export => _ => _
        .Executes(() =>
        {
            Unity(s => s
                .EnableBatchMode()
                .EnableQuit()
                .SetProjectPath(Solution.Directory)
                .SetExecuteMethod("Editor.Exporter.Android"));
        });
}
