using UnityEditor;

namespace Editor
{
    // ReSharper disable once UnusedType.Global
    public static class Exporter
    {
        // ReSharper disable once UnusedMember.Global
        public static void Android()
        {
            EditorUserBuildSettings.exportAsGoogleAndroidProject = true;

            BuildPipeline.BuildPlayer(new[] { "Assets/Scenes/SampleScene.unity" },
                "build/android",
                BuildTarget.Android,
                BuildOptions.None);
        }
    }
}